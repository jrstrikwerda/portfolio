import React from "react";

function Footer() {
  return (
    <div className="footer-wrapper">
      <div className="footer">
        <div className="copyright">
          © {new Date().getFullYear()} JR Strikwerda
        </div>
      </div>
    </div>
  );
}

export default Footer;
