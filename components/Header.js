import React from "react";

import styles from "../styles/components/Header.module.css";

function Header({
  headerImage = "/no-image.png",
  headerText = "Header Text",
  headerSubText = "",
}) {
  return (
    <>
      <div className={styles.cover}></div>
      <div className={styles.header + " padding-50"}>
        <div className={styles.photo}>
            <img src={headerImage} alt="no-image" className={styles.photo_image}/>
        </div>
        <div className={styles.header_text}>
          <h1>{headerText}</h1>
          <p>{headerSubText}</p>
        </div>
      </div>
    </>
  );
}

export default Header;
