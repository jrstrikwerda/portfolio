import React from "react";
import Link from "next/link";

import styles from "../styles/components/Logo.module.css";

function Logo() {
  return (
    // Alternatively <div className="logo">
    <div className={styles.logo}>
      <Link href="/">
        <a>
          {/* <img src="/logo.png" alt="Logo" /> */}
          Jorrit Strikwerda
        </a>
      </Link>
      {/* <style jsx>{`
        .logo img {
          display: block;
          width: 120px;
        }
        @media (max-width: 600px) {
          .logo {
            display: inline-block;
          }
        }
      `}</style> */}
    </div>
  );
}

export default Logo;
