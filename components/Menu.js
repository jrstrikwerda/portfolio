import React from "react";
import Link from "next/link";
import styles from "../styles/components/Menu.module.css";

function Menu() {
  return (
    <ul className={styles.menu}>
      <li>
        <Link href="/">
          <a>Home</a>
        </Link>
      </li>
      <li>
        <Link href="/services">
          <a>Services</a>
        </Link>
      </li>
      <li>
        <Link href="/portfolio">
          <a>Portfolio</a>
        </Link>
      </li>
      <li>
        <Link href="/about">
          <a>About</a>
        </Link>
      </li>
    </ul>
  );
}

export default Menu;
