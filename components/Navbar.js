import React, { useState, useEffect } from "react";

import Logo from "./Logo";
import Menu from "./Menu";

function Navbar() {
  let listener = null;
  const [scrollState, setScrollState] = useState("");

  useEffect(() => {
    listener = document.addEventListener("scroll", (e) => {
      var scrolled = document.scrollingElement.scrollTop;
      if (scrolled >= 40) {
        if (scrollState !== "scroll") {
          setScrollState("scroll");
          // document.querySelector(".content-wrapper").className = "content-wrapper mt-space-navbar"
        }
      } else {
        if (scrollState !== "") {
          setScrollState("");
          // document.querySelector(".content-wrapper").className = "content-wrapper"
        }
      }
    });
    return () => {
      document.removeEventListener("scroll", listener);
    };
  }, [scrollState]);

  return (
    <div className={"navbar-wrapper " + scrollState}>
      <div className={"navbar"}>
        <Logo />
        <Menu />
      </div>
    </div>
  );
}

export default Navbar;
