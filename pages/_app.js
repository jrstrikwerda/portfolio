import "../styles/globals.css";
import Navbar from "../components/Navbar";
import Footer from "../components/Footer";

function MyApp({ Component, pageProps }) {
  return (
    <div className="site-wrapper">
      <Navbar />
      <div className="content-wrapper">
        <div className="content">
          <Component {...pageProps} />
        </div>
      </div>

      <Footer />
    </div>
  );
}

export default MyApp;
