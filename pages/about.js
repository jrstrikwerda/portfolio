import React from "react";
import styles from "../styles/About.module.css";

import Header from "../components/Header";

function About() {
  return (
    <div className="about">
      <Header headerText="About Me" headerImage="/ik.jpg" />
      <div className="padding-50">
        <div className={styles.about_me}>
          <h3>Een korte introductie!</h3>
          <p>
            Hey! Mijn naam is Jorrit en ik ben een Software Engineer met een
            passie voor back end development. Ik streef naar een carriere waarin ik
            mijn interesse voor de zorg en de ICT samen kan brengen.
            <br />
            <br />
            Als ik niet achter de computer zit, haal ik plezier uit het spelen
            van bordspellen, skiën en koken.
          </p>
        </div>
      </div>
    </div>
  );
}

export default About;
