import React from "react";
import styles from "../styles/Services.module.css";

import Header from "../components/Header";

function Services() {
  return (
    <div className="services">
      <Header headerText="Diensten" headerImage="/pngegg.png" />
      <div className="padding-50">
        <div className={styles.about_me}>
          <h3>Mijn Diensten</h3>
          <div id="lipsum">
            <p>
              Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent
              faucibus leo quis laoreet malesuada. Phasellus porta magna orci,
              finibus egestas nisl tincidunt ut. Pellentesque ultricies blandit
              ipsum, sed elementum urna interdum ut. Ut mauris mauris, tristique
              at ornare in, dictum et erat. Nulla facilisi. Vivamus id nisi
              sodales, finibus nisi at, mattis est. Sed eget velit ut risus
              malesuada elementum. Nunc finibus ipsum vitae metus varius, in
              finibus mauris ullamcorper. Pellentesque in dui nec diam
              scelerisque porta sit amet at turpis. Nullam vitae mauris
              tincidunt, volutpat ante id, feugiat ex. Nam vitae dignissim ex.
              Vestibulum ligula enim, iaculis vel placerat in, pellentesque sit
              amet sem. Phasellus molestie eros in metus imperdiet varius.
            </p>
            <p>
              Pellentesque habitant morbi tristique senectus et netus et
              malesuada fames ac turpis egestas. Nullam sed enim condimentum,
              tempus magna ac, tincidunt lacus. Sed ultricies lacus vel magna
              commodo, at pretium nulla imperdiet. Aenean libero quam, imperdiet
              et nibh ac, elementum consectetur elit. Fusce ornare ut sapien in
              iaculis. Fusce viverra justo urna, at tincidunt massa fermentum
              id. Donec eu mauris sem. Duis vel ornare orci.
            </p>
            <p>
              Etiam maximus, erat ac mollis vehicula, enim lacus viverra orci,
              vitae porttitor risus odio ut ligula. Quisque a quam blandit,
              molestie ipsum eget, egestas mauris. Vivamus dictum enim ut erat
              imperdiet, at pretium nunc ultricies. Pellentesque condimentum,
              turpis eget molestie commodo, tortor dolor lobortis ante, a
              feugiat leo justo eu lacus. Vestibulum interdum tellus convallis
              est vestibulum, id euismod velit condimentum. Proin auctor
              bibendum condimentum. Fusce ornare risus quis lorem fringilla
              laoreet. Vivamus tempor turpis id orci pharetra fermentum. In
              vitae placerat tellus, quis maximus lorem. Integer dapibus
              consequat eros, vel semper diam tristique et. Sed suscipit ante et
              erat finibus, tristique auctor sapien maximus. Fusce ut leo metus.
              Curabitur eu fringilla lectus. Ut a risus quis orci mattis aliquam
              sed id justo. Vestibulum id lobortis risus, eu condimentum quam.
            </p>
          </div>
        </div>
      </div>
    </div>
  );
}

export default Services;
